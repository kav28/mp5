// Import necessary dependencies
use serde_json::Value;
use reqwest::Error as ReqwestError; // Import ReqwestError for HTTP requests
use serde::Deserialize;
use rusoto_core::{Region, RusotoError};
use rusoto_dynamodb::{DynamoDb, DynamoDbClient, PutItemInput, AttributeValue};
use std::collections::HashMap;
use lambda_runtime::{Error, lambda, Context};
use serde_json::{json, Value};
use aws_xray_sdk::plugins::DefaultSegment;
use aws_xray_sdk::sdk::TraceRecorder;

// Define a struct to represent the data from the API response
#[derive(Debug, Deserialize)]
struct WaterQualityData {
    WaterPollution: f64,
    // Add other fields as needed
}

// Initialize DynamoDB client
fn init_dynamodb_client() -> DynamoDbClient {
    DynamoDbClient::new(Region::default())
}

// Store data in DynamoDB
async fn store_data_in_dynamodb(data: Vec<WaterQualityData>, client: DynamoDbClient) -> Result<(), RusotoError<PutItemInput>> {
    for record in data {
        let mut item = HashMap::new();
        // Populate the item attributes
        // Convert string to AttributeValue
        item.insert("WaterPollution".to_string(), AttributeValue {
            s: Some(record.WaterPollution.to_string()), // Convert string to AttributeValue
            ..Default::default() // Add other fields as needed
        });
        // Add other fields as needed
        let input = PutItemInput {
            table_name: "WaterQual".to_string(), 
            item,
            ..Default::default()
        };
        // Put item into DynamoDB table
        match client.put_item(input).await {
            Ok(_) => println!("Successfully stored item in DynamoDB"),
            Err(err) => eprintln!("Error storing item in DynamoDB: {:?}", err),
        }
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Initialize AWS X-Ray SDK
    aws_xray_sdk::init()
        .map_err(|e| format!("Failed to initialize X-Ray SDK: {:?}", e))?;
    
    // Logging when the Lambda function starts
    println!("Lambda function invoked");

    // Define the URL of the API endpoint
    let api_url = "https://www.kaggle.com/datasets/xontoloyo/exploring-water-quality?select=water_quality.csv";

    // Send a GET request to the API endpoint
    let response = reqwest::get(api_url)
        .await
        .map_err(|e| ReqwestError::from(e))?;

    // Check if the request was successful
    if response.status().is_success() {
        // Logging when processing the event
        println!("Processing the event");

        // Parse the response body as JSON
        let data: Vec<WaterQualityData> = response
            .json::<Vec<WaterQualityData>>()
            .await
            .map_err(|e| ReqwestError::from(e))?;

        // Initialize DynamoDB client
        let client = init_dynamodb_client();

        // Store data in DynamoDB
        if let Err(err) = store_data_in_dynamodb(data, client).await {
            return Err(Box::new(err));
        }

        println!("Data stored in DynamoDB");
    } else {
        // Logging when there is an error in the request
        eprintln!("Error: Request to the API endpoint was not successful. Status: {}", response.status());
        // Return an error response if the request was not successful
        return Err(Box::new(ReqwestError::from(response.status())));
    }
    Ok(())
}
