use reqwest::Error as ReqwestError; // Import ReqwestError for HTTP requests
use reqwest::StatusCode;
use rusoto_core::{Region, RusotoError};
use rusoto_dynamodb::{DynamoDb, DynamoDbClient, PutItemInput, PutItemError, AttributeValue};
use serde::Deserialize;
use std::collections::HashMap;
use std::fmt;

// Define a custom error type to hold various error types
#[derive(Debug)]
enum CustomError {
    Reqwest(ReqwestError),
    StatusCode(StatusCode),
    Rusoto(RusotoError<PutItemError>),
    SerializationError(serde_json::Error),
}

impl fmt::Display for CustomError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            CustomError::Reqwest(e) => write!(f, "Reqwest error: {}", e),
            CustomError::StatusCode(s) => write!(f, "HTTP status code error: {}", s),
            CustomError::Rusoto(e) => write!(f, "Rusoto error: {}", e),
            CustomError::SerializationError(e) => write!(f, "Serialization error: {}", e),
        }
    }
}

impl std::error::Error for CustomError {}

impl From<ReqwestError> for CustomError {
    fn from(error: ReqwestError) -> Self {
        CustomError::Reqwest(error)
    }
}

impl From<StatusCode> for CustomError {
    fn from(status_code: StatusCode) -> Self {
        CustomError::StatusCode(status_code)
    }
}

impl From<RusotoError<PutItemError>> for CustomError {
    fn from(error: RusotoError<PutItemError>) -> Self {
        CustomError::Rusoto(error)
    }
}

impl From<serde_json::Error> for CustomError {
    fn from(error: serde_json::Error) -> Self {
        CustomError::SerializationError(error)
    }
}

// Define a struct to represent the data from the API response
#[derive(Debug, Deserialize)]
struct WaterQualityData {
    WaterPollution: f64,
    // Add other fields as needed
}

// Initialize DynamoDB client
fn init_dynamodb_client() -> DynamoDbClient {
    DynamoDbClient::new(Region::default())
}

// Store data in DynamoDB
async fn store_data_in_dynamodb(data: Vec<WaterQualityData>, client: DynamoDbClient) -> Result<(), CustomError> {
    for record in data {
        let mut item = HashMap::new();
        item.insert("AttributeName".to_string(), AttributeValue {
            s: Some("attribute_value".to_string()), // Insert string attribute
            ..Default::default()
        });
        // Add other fields as needed
        let input = PutItemInput {
            table_name: "WaterQual".to_string(),
            item,
            ..Default::default()
        };
        // Put item into DynamoDB table
        client.put_item(input).await?;
        println!("Successfully stored item in DynamoDB");
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), CustomError> {
    // Define the URL of the API endpoint
    let api_url = "https://www.kaggle.com/datasets/xontoloyo/exploring-water-quality?select=water_quality.csv";

    // Send a GET request to the API endpoint
    let response = reqwest::get(api_url).await?;

    // Check if the request was successful
    if response.status().is_success() {
        // Get the response body as text
        let json_data = response.text().await?;

        // Deserialize the JSON data into your struct
        let data: Vec<WaterQualityData> = serde_json::from_str(&json_data)?;

        // Initialize DynamoDB client
        let client = init_dynamodb_client();

        // Store data in DynamoDB
        store_data_in_dynamodb(data, client).await?;

        println!("Data stored in DynamoDB");
    } else {
        // Return an error response if the request was not successful
        return Err(response.status().into());
    }
    Ok(())
}




